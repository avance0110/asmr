package com.matome.asmr;

import android.content.Context;
import android.content.SharedPreferences;

public  class Util {

    public static String refreshTag;
    public static final String YOUTUBE_WORD ="word";
    public static final String YOUTUBE_ORDER ="oder";

    public static final int VIEW_TYPE_TAB2 = 1;
    public static final int VIEW_TYPE_AD = 101;
    public static final int NEND_TAB = 102;
    public static final int NEND_LIST = 103;





    //設定保存
    public static void setSave(Context con, String value, String key){
        SharedPreferences pref = con.getSharedPreferences("DataSave", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=pref.edit();
        editor.putString(key,value);
        editor.commit();
    }

    //設定取得
    public static String getSerch(Context con, String key){
        SharedPreferences pref = con.getSharedPreferences("DataSave", Context.MODE_PRIVATE);
        String def = con.getString(R.string.youtube_word);
        String token = pref.getString(key, def);
        return token;
    }

    public static String getOrder(Context con, String key){
        SharedPreferences pref = con.getSharedPreferences("DataSave", Context.MODE_PRIVATE);
        String def = con.getString(R.string.youtube_order);
        String token = pref.getString(key, def);
        return token;
    }

}