package com.matome.asmr.youtube;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

//import com.google.android.gms.ads.AdRequest;
//import com.google.android.gms.ads.NativeExpressAdView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.matome.asmr.R;

public class YoutubeRecyAdapter extends RecyclerView.Adapter<YoutubeRecyAdapter.BaseViewHolder> {
    final String TAG = "YoutubeRecyAdapter";
    private LayoutInflater mInflater;
    private ArrayList<YouTubeData> mData;
    private Context mContext;
    private static final int LIST_AD_DATA = 1000;
    private static final int CONTENT = 0;
    private static final int AD = 1;

    public YoutubeRecyAdapter(ArrayList<YouTubeData> data, Context context) {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mData = data;
    }
    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mInflater = LayoutInflater.from(parent.getContext());
        // 表示するレイアウトを設定
        switch (viewType) {
            case CONTENT:
                return new ListViewHolder(mInflater.inflate(R.layout.youtube_list_row, parent, false));
//            case AD:
//                return new AdRecyclerHolder(mInflater.inflate(R.layout.recycler_list_ad, parent, false));
        }
        return null;
    }
    @Override
    public void onBindViewHolder(final BaseViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case CONTENT:
                // 特定の行(position)のデータを得る
                YouTubeData item = mData.get(getRealPosition(position));
//                YouTubeData item = getItem(position);

                // YoutubeDataのデータをViewの各Widgetにセットする
                //タイトル
                holder.memberTitleText.setText(item.getTitleText());
                //チャンネル名
                holder.channelName.setText(item.getChannelTitle());
                //公開日
                holder.memberPubDate.setText(item.getPubData());
                //再生時間
                holder.timeText.setText(item.getPlayTime());
                //再生回数
                holder.viewCount.setText(item.getViewCount());
                //サムネイル画像取得
                holder.url = item.getImageUrl();

                try{
//                    Log.v(TAG," getImageUrl = " + item.getImageUrl() );
                    if(item.getImageUrl() != null){
                        Picasso.with(mContext).cancelRequest(holder.memberImage);
                        Picasso.with(mContext).load(holder.url)
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .fit()
                                .centerCrop()
                                .into(holder.memberImage);
                        holder.memberImage.setVisibility(View.VISIBLE);
//                        Picasso.with(mContext).setIndicatorsEnabled(true);
                    }else{
                        //課題:スクロールすると間違った位置に画像がセットされる
                        //Picassoで使用しているsetImageDrawableにnullを入れる
                        holder.memberImage.setImageDrawable(null);
                    }
                }catch (Exception e){
                    //課題:スクロールすると間違った位置に画像がセットされる
                    //Picassoで使用しているsetImageDrawableにnullを入れる
                    holder.memberImage.setImageDrawable(null);
                    Log.e(TAG," not Path = " + item.getImageUrl());
                }
                Picasso.with(mContext).invalidate(item.getImageUrl());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        YouTubeData data = mData.get(getRealPosition(holder.getAdapterPosition()));
                        Log.e(TAG,"data=" + data.getTitleText());
                        String videoId = data.getVideoId();
                        Log.e(TAG,"videoId=" + videoId);
                        YoutubeFragment.youTubePlayer.loadVideo(videoId);
                        YoutubeFragment.youTubePlayer.play();
                    }
                });
                break;
            case AD:
//                AdRequest adRequest = new AdRequest.Builder().build();
//                holder.mAdView.loadAd(adRequest);
                break;
            default:
                break;
        }
    }
    private int getRealPosition(int position) {
        if (LIST_AD_DATA == 0) {
            return position;
        } else {
            return position - position / LIST_AD_DATA ;
        }
    }
    @Override
    public int getItemCount() {
        int adContent = 0;
        if (mData != null && mData.size() > 0 && LIST_AD_DATA > 0) {
            adContent = mData.size() / LIST_AD_DATA;
        }
        if(mData != null){
            return mData.size() + adContent;
        }else {
            return 0;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position > 0 && LIST_AD_DATA > 0 && position % LIST_AD_DATA == 0) {
            return AD;
        }
        return CONTENT;
    }
    public class BaseViewHolder extends RecyclerView.ViewHolder {
        ImageView memberImage;
        TextView memberPubDate;
        TextView memberTitleText;
        TextView channelName;
        TextView viewCount;
        TextView timeText;
        String url ;

//        NativeExpressAdView mAdView;
        public BaseViewHolder(View v) {
            super(v);
        }
    }
    public class ListViewHolder extends BaseViewHolder{
        public ListViewHolder(View v) {
            super(v);
            memberImage = (ImageView)v.findViewById(R.id.row_image);
            memberTitleText = (TextView)v.findViewById(R.id.title);
            memberPubDate = (TextView)v.findViewById(R.id.date_text);
            channelName = (TextView)v.findViewById(R.id.channel_text);
            timeText = (TextView)v.findViewById(R.id.time_text);
            viewCount = (TextView)v.findViewById(R.id.view_count);
        }
    }
    public class AdRecyclerHolder extends BaseViewHolder{
        public AdRecyclerHolder(View v) {
            super(v);
//            mAdView = (NativeExpressAdView) v.findViewById(R.id.adView);
        }
    }
}
