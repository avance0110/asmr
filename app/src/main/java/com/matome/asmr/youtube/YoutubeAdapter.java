package com.matome.asmr.youtube;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import com.matome.asmr.R;


public class YoutubeAdapter extends ArrayAdapter<YouTubeData> {
    private LayoutInflater layoutInflater_;
    private Context mContext;

    public YoutubeAdapter(Context context, int textViewResourceId, List<YouTubeData> objects) {
        super(context, textViewResourceId, objects);
        mContext = context;
        layoutInflater_ = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // 特定の行(position)のデータを得る
        YouTubeData item = getItem(position);
        ViewHolder viewHolder;

        // convertViewは使い回しされている可能性があるのでnullの時だけ新しく作る
        if (null == convertView) {
        }

        //最初 or 再利用するものがなかったら
        if(convertView == null){
            //レイアウトを指定
            convertView = layoutInflater_.inflate(R.layout.youtube_list_row, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }


        // YoutubeDataのデータをViewの各Widgetにセットする
        //タイトル
        viewHolder.memberTitleText.setText(item.getTitleText());
        //チャンネル名
        viewHolder.channelName.setText(item.getChannelTitle());
        //公開日
        viewHolder.memberPubDate.setText(item.getPubData());
        //再生時間
        viewHolder.timeText.setText(item.getPlayTime());
        //再生回数
        viewHolder.viewCount.setText(item.getViewCount());



        //サムネイル画像取得
        viewHolder.url = item.getImageUrl();
        Picasso.with(mContext)
                .load(viewHolder.url)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .fit()
                .centerCrop()
                .into(viewHolder.memberImage);
        return convertView;
    }

    //ViewHolder
    private  class ViewHolder{

        ImageView memberImage;
        TextView memberPubDate;
        TextView memberTitleText;
        TextView channelName;
        TextView viewCount;
        TextView timeText;



        String url ;

        public ViewHolder(View view){

            this.memberImage = (ImageView)view.findViewById(R.id.row_image);
            this.memberTitleText = (TextView)view.findViewById(R.id.title);
            this.memberPubDate = (TextView)view.findViewById(R.id.date_text);
            this.channelName = (TextView)view.findViewById(R.id.channel_text);
            this.timeText = (TextView)view.findViewById(R.id.time_text);
            this.viewCount = (TextView)view.findViewById(R.id.view_count);
        }
    }
}